//
//  Alerta.swift
//  Siga Denuncia
//
//  Created by Geoambiente Sensoriamento Remoto LTDA on 28/07/15.
//  Copyright (c) 2015 Geoambiente Sensoriamento Remoto LTDA. All rights reserved.
//

import UIKit

class Alerta {
    class func show(msg: String, title: String, viewController: UIViewController){
        var alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        viewController.presentViewController(alert, animated: true, completion: nil)
    }
}
