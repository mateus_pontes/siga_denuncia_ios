//
//  ItemMenu.swift
//  Siga Denuncia
//
//  Created by Geoambiente Sensoriamento Remoto LTDA on 28/07/15.
//  Copyright (c) 2015 Geoambiente Sensoriamento Remoto LTDA. All rights reserved.
//

import Foundation
class ItemMenu{
    var target: String = ""
    var label: String = ""
    var hint:String = ""
    var image: String = ""
}