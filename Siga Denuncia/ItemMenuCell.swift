//
//  ItemMenuCell.swift
//  Siga Denuncia
//
//  Created by Geoambiente Sensoriamento Remoto LTDA on 28/07/15.
//  Copyright (c) 2015 Geoambiente Sensoriamento Remoto LTDA. All rights reserved.
//

import UIKit
class ItemMenuCell: UITableViewCell {
    @IBOutlet var cellName : UILabel!
    @IBOutlet var cellHint : UILabel!
    @IBOutlet var cellImg : UIImageView!
    
}