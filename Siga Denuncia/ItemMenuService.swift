//
//  ItemMenuService.swift
//  Siga Denuncia
//
//  Created by Geoambiente Sensoriamento Remoto LTDA on 28/07/15.
//  Copyright (c) 2015 Geoambiente Sensoriamento Remoto LTDA. All rights reserved.
//

import Foundation
class ItemMenuService{
    class func getItemMenu() -> Array<ItemMenu>{
        var source: Array<ItemMenu> = []
        
        var item =  ItemMenu()
        item =  ItemMenu()
        item.target = "novaDenuncia"
        item.label = "Criar Denúncia"
        item.hint = "Colabore criando novas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "minhasDenuncias"
        item.label = "Minhas Denúncias"
        item.hint = "Visualize suas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "mapaDenuncia"
        item.label = "Mapa Denúncias"
        item.hint = "Visualize no mapa as suas denúncias"
        source.append(item)
        
        return source
    }
    
    class func getItemMenuTipoInfracao() -> Array<ItemMenu>{
        var source: Array<ItemMenu> = []
        
        var item =  ItemMenu()
        item =  ItemMenu()
        item.target = "desmatamentoIlegal"
        item.label = "Desmatamento Ilegal"
        item.hint = "Colabore criando novas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "cavaIlegal"
        item.label = "Cava Ilegal"
        item.hint = "Visualize suas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "poluicao"
        item.label = "Poluição"
        item.hint = "Visualize no mapa as suas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "residuoDomestico"
        item.label = "Resíduo Doméstico"
        item.hint = "Visualize no mapa as suas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "residuoConstrucaoCivil"
        item.label = "Resíduo Construção Cívl"
        item.hint = "Visualize no mapa as suas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "residuoHospitalar"
        item.label = "Resíduo Hospitalar"
        item.hint = "Visualize no mapa as suas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "residuoLenhoso"
        item.label = "Resíduo Lenhoso"
        item.hint = "Visualize no mapa as suas denúncias"
        source.append(item)
        
        item =  ItemMenu()
        item.target = "outro"
        item.label = "Outros"
        item.hint = "Visualize no mapa as suas denúncias"
        source.append(item)
        
        return source
    }
}