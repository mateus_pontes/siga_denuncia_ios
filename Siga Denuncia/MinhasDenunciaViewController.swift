//
//  MinhasDenunciaViewController.swift
//  Siga Denuncia
//
//  Created by Geoambiente Sensoriamento Remoto LTDA on 28/07/15.
//  Copyright (c) 2015 Geoambiente Sensoriamento Remoto LTDA. All rights reserved.
//

import UIKit

class MinhasDenunciaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Minhas Denúncias"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
