//
//  SobreViewController.swift
//  Siga Denuncia
//
//  Created by Geoambiente Sensoriamento Remoto LTDA on 24/07/15.
//  Copyright (c) 2015 Geoambiente Sensoriamento Remoto LTDA. All rights reserved.
//

import UIKit

class SobreViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet var webView : UIWebView!
    @IBOutlet var progress : UIActivityIndicatorView!
    
    let URL_SOBRE = "http://geoambiente.com.br"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Sobre"
        
        self.progress.startAnimating()
        var url = NSURL(string: URL_SOBRE)
        var request = NSURLRequest(URL: url!)
        self.webView.loadRequest(request)
        webView.delegate = self
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.progress.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
