//
//  TiposDenunciaViewController.swift
//  Siga Denuncia
//
//  Created by Geoambiente Sensoriamento Remoto LTDA on 28/07/15.
//  Copyright (c) 2015 Geoambiente Sensoriamento Remoto LTDA. All rights reserved.
//

import UIKit

class TiposDenunciaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableview : UITableView!
    var sourceItemsMenu: Array<ItemMenu> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Criar Nova Deúncia"
        sourceItemsMenu = ItemMenuService.getItemMenuTipoInfracao()
        
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        var xib = UINib(nibName: "ItemMenuCell", bundle: nil)
        self.tableview.registerNib(xib, forCellReuseIdentifier: "cell")
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sourceItemsMenu.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->UITableViewCell
    {
        let cell =  self.tableview.dequeueReusableCellWithIdentifier("cell") as! ItemMenuCell
        let linha = indexPath.row
        let menu = self.sourceItemsMenu[linha]
        
        cell.cellName.text = menu.label
        cell.cellHint.text = ""
        cell.cellImg.image = UIImage(named: "ferrari_ff.png")
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let linha = indexPath.row
        
        if (sourceItemsMenu[linha].target == "minhasDenuncias"){
            let vc = MinhasDenunciaViewController(nibName: "MinhasDenunciaViewController", bundle: nil)
            self.navigationController!.pushViewController(vc, animated: true)
        }
        else if (sourceItemsMenu[linha].target == "mapaDenuncia"){
            let vc = MapaViewController(nibName: "MapaViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let vc = TiposDenunciaViewController(nibName: "TiposDenunciaViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
